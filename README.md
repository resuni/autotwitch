# autotwitch.py

## Installation & Configuration

1. Download or clone this repository.

 `git clone https://github.com/resuni/autotwitch.git`

2. Copy config.json.example to config.json.

 `cp config.json.example config.json`

3. Follow the instructions at https://blog.twitch.tv/client-id-required-for-kraken-api-calls-afbb8e95f843 to generate your own Twitch OAuth token. Paste this token in the "oauth" section of config.json.
4. Change the "game" parameter in config.json to your preferred game.
5. Run the script.

 `python3 autotwitch.py`

5. Enjoy your stream!

## Workaround for "Failed to authenticate, the access token is invalid or missing required scope"

The stream may fail to open with the following error message:

```
[plugin.twitch][info] Attempting to authenticate using OAuth token
[plugin.twitch][error] Failed to authenticate, the access token is invalid or missing required scope
error: Unable to open URL: https://api.twitch.tv/api/channels/blapatureco/access_token.json (400 Client Error: Bad Request for url: ...
```
This is caused by Twitch's recent requirement of OAuth tokens on every API request. You can use this workaround if you are experiencing this problem: 

1. Obtain livestreamer's Twitch OAuth token using the workaround described in accounttest's response to this issue (posted on Sep 29, 2016; the very last post): https://github.com/chrippa/livestreamer/issues/1456
2. Paste "--twitch-oauth-authenticate \<OAuth Token\>" at the end of the "append" string in config.json.
3. Run the script.

## Reference for config.json

* "oauth"	Twitch oauth string
* "game"	Game to look up using Twitch API
* "streamer"	Path to executable to launch stream
* "append"	Options and parameters to append to the command

Script generates command as follows:

`<config.json "streamer"> <Twitch URL as calculated by the script> <config.json "append">` 
