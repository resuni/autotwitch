import json			# For parsing JSON
import urllib.parse		# For encoding the URL
import requests			# For making the HTTP request
import os			# For executing the shell command

# Load configuration from file config.json
with open('config.json') as config_file:
 config = json.load(config_file)

# Make name of game URL-friendly
game = urllib.parse.quote_plus(config["game"])

# Connect to Twitch API and get latest stream information
url = "https://api.twitch.tv/kraken/streams?game={0}&limit=1".format(game)
headers = {'Accept': 'application/vnd.twitchtv.v2+json', 'Client-ID': config["oauth"]}
r = requests.get(url, headers=headers)
json_data = json.loads(r.content.decode("utf-8"))
url = json_data['streams'][0]['channel']['url']

# Execute command
os.system(config["streamer"]+" "+url+" "+config["append"])

# Since this is my first time using Python, here's a list of all the articles I had to stumble through.
# JSON parsing:		http://docs.python-guide.org/en/latest/scenarios/json/
# More JSON parsing:	http://stackoverflow.com/a/2835672/6491246
# URL encoding:		http://stackoverflow.com/a/9345102/6491246
# Variables in strings:	http://stackoverflow.com/a/2962966/6491246
# HTTP requests:	http://stackoverflow.com/a/15869929/6491246
# More HTTP requests:	http://docs.python-requests.org/en/master/user/quickstart/
# Bytes to string:	http://stackoverflow.com/a/606199/6491246
# Exec shell command:	http://stackoverflow.com/a/92395/6491246
